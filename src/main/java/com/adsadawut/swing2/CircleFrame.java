/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing2;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class CircleFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Circle");
        frame.setSize(300,300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblRadius = new JLabel("Radius:",JLabel.TRAILING);
        lblRadius.setSize(50,20);
        lblRadius.setLocation(5,5);
        lblRadius.setBackground(Color.WHITE);
        lblRadius.setOpaque(true);
        
        final JTextField txtRadius = new JTextField();
        txtRadius.setSize(60,20);
        txtRadius.setLocation(60,5);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100,20);
        btnCalculate.setLocation(130, 5);
        
        final JLabel lblResult = new JLabel("Circle radius = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300,50);
        lblResult.setLocation(0,100);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        //Event Driven = รันโปรแกรมตามสถานการณ์
        btnCalculate.addActionListener(new ActionListener(){ //Anonymous Class
            @Override
            public void actionPerformed(ActionEvent e) {
               try{
                //1. ดึงข้อมูล text จาก txtRadius -> strRadius
                String strRadius = txtRadius.getText();
                //2. แปลง strRadius ->  radius : Double.parseDouble
                double radius = Double.parseDouble(strRadius);//-> NumberFormatException
                //3. instance object (สร้างเป็นออปเจค ) Circle(radius) -> circle
                Circle circle = new Circle(radius);
                //4. update lblResult โดยนำข้อมูลจาก circle ไปแสดงให้ครบถ้วน
                lblResult.setText("Circle r = " + String.format("%.2f",circle.getRadius())
                        + " area = " + String.format("%.2f",circle.calArea())
                        + " perimeter = "+ String.format("%.2f",circle.calPerimeter()));
               } catch (Exception ex){
                   JOptionPane.showMessageDialog(frame, "Error : Please input Number"
                           ,"Error",JOptionPane.ERROR_MESSAGE);
                   txtRadius.setText("");
                   txtRadius.requestFocus();
               }
            }
        });
        
        
        frame.add(lblResult);
        frame.add(btnCalculate);
        frame.add(lblRadius);
        frame.add(txtRadius);
        frame.setVisible(true);
    }
}
