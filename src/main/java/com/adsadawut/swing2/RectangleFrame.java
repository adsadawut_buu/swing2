/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing2;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class RectangleFrame extends JFrame {
    JLabel lblWidth;
    JLabel lblHeight;
    JTextField txtWidth;
    JTextField txtHeight;
    JButton btnCalculate;
    JLabel lblResult;
    public RectangleFrame(){
        super("Rectangle");
        this.setSize(450, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lblWidth = new JLabel("Width:",JLabel.TRAILING);
        lblWidth.setSize(50,20);
        lblWidth.setLocation(5,5);
        lblWidth.setBackground(Color.WHITE);
        lblWidth.setOpaque(true);
        
        lblHeight = new JLabel("Height:",JLabel.TRAILING);
        lblHeight.setSize(50,20);
        lblHeight.setLocation(5,30);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        
        JTextField txtWidth = new JTextField();
        txtWidth.setSize(60,20);
        txtWidth.setLocation(60,5);
        
         JTextField txtHeight = new JTextField();
        txtHeight.setSize(60,20);
        txtHeight.setLocation(60,30);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100,20);
        btnCalculate.setLocation(130, 5);
        
        JLabel lblResult = new JLabel("Rectangle width = ??? height = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(450,50);
        lblResult.setLocation(0,100);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        //Event Driven = รันโปรแกรมตามสถานการณ์
        btnCalculate.addActionListener(new ActionListener(){ //Anonymous Class
            @Override
            public void actionPerformed(ActionEvent e) {
               try{
                //1. ดึงข้อมูล text จาก txtWidth -> strWidth และtxtHeight -> strHeight
                String strWidth = txtWidth.getText();
                String strHeight = txtHeight.getText();
                //2. แปลง strWidth ->  width : Double.parseDouble และ  strHeight ->  Height : Double.parseDouble
                double width = Double.parseDouble(strWidth);//-> NumberFormatException
                double height = Double.parseDouble(strHeight);
                //3. instance object (สร้างเป็นออปเจค ) Rectangle(width,height) -> rectangle
                Rectangle rectangle = new Rectangle(width,height);
                //4. update lblResult โดยนำข้อมูลจาก Rectangle ไปแสดงให้ครบถ้วน
                lblResult.setText("Rectangle width = " + String.format("%.2f",rectangle.getWidth())
                        + " height = " + String.format("%.2f",rectangle.getHeight())
                        + " area = " + String.format("%.2f",rectangle.calArea())
                        + " perimeter = "+ String.format("%.2f",rectangle.calPerimeter()));
               } catch (Exception ex){
                   JOptionPane.showMessageDialog(RectangleFrame.this, "Error : Please input Number"
                           ,"Error",JOptionPane.ERROR_MESSAGE);
                   txtWidth.setText("");
                   txtWidth.requestFocus();
                   txtHeight.setText("");
                   txtHeight.requestFocus();
               }
            }
        });
        
        
        this.add(lblResult);
        this.add(btnCalculate);
        this.add(lblWidth);
        this.add(txtWidth);
        this.add(lblHeight);
        this.add(txtHeight);
    }

    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }
}
