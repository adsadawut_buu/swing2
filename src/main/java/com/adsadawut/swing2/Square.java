/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing2;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class Square extends Shape{
    private double side;

    public Square(double side) {
        super("Square:");
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
    
    
    @Override
    public double calArea() {
        return Math.pow(side, 2);
    }

    @Override
    public double calPerimeter() {
        return 4*side;
    }
    
}
