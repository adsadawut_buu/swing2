/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing2;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class SquareFrame extends JFrame {

    JLabel lblSide;
    JTextField txtSide;
    JButton btnCalculate;
    JLabel lblResult;

    public SquareFrame() {
        super("Square");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblSide = new JLabel("Side:", JLabel.TRAILING);
        lblSide.setSize(50, 20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);

        JTextField txtRadius = new JTextField();
        txtRadius.setSize(60, 20);
        txtRadius.setLocation(60, 5);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(130, 5);

        JLabel lblResult = new JLabel("Square side = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 100);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        //Event Driven = รันโปรแกรมตามสถานการณ์
        btnCalculate.addActionListener(new ActionListener() { //Anonymous Class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    //1. ดึงข้อมูล text จาก txtSide -> strRadius
                    String strRadius = txtRadius.getText();
                    //2. แปลง strRadius ->  radius : Double.parseDouble
                    double side = Double.parseDouble(strRadius);//-> NumberFormatException
                    //3. instance object (สร้างเป็นออปเจค ) Circle(radius) -> circle
                    Square square = new Square(side);
                    //4. update lblResult โดยนำข้อมูลจาก circle ไปแสดงให้ครบถ้วน
                    lblResult.setText("Circle r = " + String.format("%.2f", square.getSide())
                            + " area = " + String.format("%.2f", square.calArea())
                            + " perimeter = " + String.format("%.2f", square.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(SquareFrame.this, "Error : Please input Number",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();
                }
            }
        });

        this.add(lblResult);
        this.add(btnCalculate);
        this.add(lblSide);
        this.add(txtRadius);
    }

    public static void main(String[] args) {
        SquareFrame frame = new SquareFrame();
        frame.setVisible(true);
    }
}
