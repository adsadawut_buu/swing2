/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing2;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class TriangleFrame extends JFrame{
    JLabel lblBase;
    JLabel lblHeight;
    JTextField txtBase;
    JTextField txtHeight;
    JButton btnCalculate;
    JLabel lblResult;
    public TriangleFrame(){
        super("Tritangle");
        this.setSize(450, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lblBase = new JLabel("Base:",JLabel.TRAILING);
        lblBase.setSize(50,20);
        lblBase.setLocation(5,5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        
        lblHeight = new JLabel("Height:",JLabel.TRAILING);
        lblHeight.setSize(50,20);
        lblHeight.setLocation(5,30);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        
        JTextField txtBase = new JTextField();
        txtBase.setSize(60,20);
        txtBase.setLocation(60,5);
        
         JTextField txtHeight = new JTextField();
        txtHeight.setSize(60,20);
        txtHeight.setLocation(60,30);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100,20);
        btnCalculate.setLocation(130, 5);
        
        JLabel lblResult = new JLabel("Triangle base = ??? height = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(450,50);
        lblResult.setLocation(0,100);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        //Event Driven = รันโปรแกรมตามสถานการณ์
        btnCalculate.addActionListener(new ActionListener(){ //Anonymous Class
            @Override
            public void actionPerformed(ActionEvent e) {
               try{
                //1. ดึงข้อมูล text จาก txtBase -> strBase และtxtHeight -> strHeight
                String strBase = txtBase.getText();
                String strHeight = txtHeight.getText();
                //2. แปลง strBase ->  base : Double.parseDouble และ  strHeight ->  Height : Double.parseDouble
                double base = Double.parseDouble(strBase);//-> NumberFormatException
                double height = Double.parseDouble(strHeight);
                //3. instance object (สร้างเป็นออปเจค ) Triangle(base,height) -> triangle
                Triangle triangle = new Triangle(base,height);
                //4. update lblResult โดยนำข้อมูลจาก Rectangle ไปแสดงให้ครบถ้วน
                lblResult.setText("Triangle base = " + String.format("%.2f",triangle.getBase())
                        + " height = " + String.format("%.2f",triangle.getHeight())
                        + " area = " + String.format("%.2f",triangle.calArea())
                        + " perimeter = "+ String.format("%.2f",triangle.calPerimeter()));
               } catch (Exception ex){
                   JOptionPane.showMessageDialog(TriangleFrame.this, "Error : Please input Number"
                           ,"Error",JOptionPane.ERROR_MESSAGE);
                   txtBase.setText("");
                   txtBase.requestFocus();
                   txtHeight.setText("");
                   txtHeight.requestFocus();
               }
            }
        });
        
        
        this.add(lblResult);
        this.add(btnCalculate);
        this.add(lblBase);
        this.add(txtBase);
        this.add(lblHeight);
        this.add(txtHeight);
    }

    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }
}
